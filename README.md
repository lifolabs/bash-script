# Bash_Script

## Examples of bash script syntax and how to learn them

Table of content:

- [Basics](00_basics/README.md)
- [Intro](01_scripting_intro/README.md)
- [Paramenters](02_scripting_paramaters/README.md)
- [Conditions and Loops](03_scripting_loops/README.md)
- [Arrays](04_arrays/README.md)
- [Functions](05_shell_funcs/README.md)
- [Graphics - Terminal](06_tui_gui/tui/README.md)
- [Graphics - Desktop](06_tui_gui/gui/README.md)
- [Class Tasks](98_tasks/README.md)
- [Home Work](99_homework/README.md)


---
&copy; All right reserved to Alex M. Schapelle and distributed under GPLv3 License as attached to project.
